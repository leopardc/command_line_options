//
//  main.cpp
//  CommandLineOptions
//
//  Created by Leonard Coster on 7/2/21.
//

#include <iostream>

/** ------------------------------------------------------------------------------------------------
 1. Include the CommandLine header.                                                               */

#include "CommandLine.hpp"


int main(int argc, const char * argv[]) {

    /** --------------------------------------------------------------------------------------------
     2. Declare a CommandLine object.                                                             */

    CommandLine cl(argc, argv);


    /** --------------------------------------------------------------------------------------------
     3. Add some switches.
        The method template will prompt you to enter the shortcut, longName, description, requried state & default
        values.
        makeSwitch returns a success bool and presents appropriate parsing errors in the console. */

    cl.makeSwitch("t", "threads", "desired number of threads", false, "4" );
    cl.makeSwitch("i", "includeFirst", "Include the first record", false, "FALSE");
    cl.makeSwitch("v", "verboseOff", "Turn verbose reporting off", false, "FALSE");
    cl.makeSwitch("V", "verboseOn", "Turn verbose reporting on", false, "TRUE");


    /** --------------------------------------------------------------------------------------------
     4. You don't have to (and will not be allowed) add help.                                     */

    if ( cl.makeSwitch("h", "help", "show me the help !", true, std::string() ) ) {
        //  returned true - the switch was added - but not in this case !
    } else {
        //  error message in console
    }


    /** --------------------------------------------------------------------------------------------
     5. Call parseCommandLine when ready.
        parseCommandLine returns a success bool and presents appropriate error messages
        for the user in the cosole.                                                               */

    cl.parseCommandLine();



    /** --------------------------------------------------------------------------------------------
     6. Access the data using the longName of the switch you want.
        The entries are stored as strings but converted to the type you want.
        If requesting a bool, getSwitchValue will look for 'true', 'True', 'TRUE', 'false', 'False'' & 'FALSE'
        Conversion to int or double is attempted with std::stoi or std::stod as appropriate.
        getSwitchValue returns true/false if the conversion to your desired type was successful.  */

    int numThreads;
    cl.getSwitchValue("threads", numThreads);

    std::string dataset;
    cl.getSwitchValue("dataset", dataset);

    bool includeFirst;
    cl.getSwitchValue("includeFirst", includeFirst);

    bool verboseOnOff;
    cl.getSwitchOnOff("v", verboseOnOff);

    std::string x = cl.getExecutablePath();

    return 0;
}
