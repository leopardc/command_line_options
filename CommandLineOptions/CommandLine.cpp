//
//  CommandLine.cpp
//  CommandLineOptions
//
//  Created by Leonard Coster on 7/2/21.
//

#include "CommandLine.hpp"


//  =============================================================================== struct CL switch

//  ----------------------------------------------------------------------------------- constructors
CommandLine::CLswitch::CLswitch()
:
shortcut(std::string() ),
longName(std::string() ),
description(std::string() ),
required(false),
value(std::string() )
{}


CommandLine::CLswitch::CLswitch(std::string _shortcut, std::string _longName, std::string _description, bool _required, std::string _value)
:
shortcut(_shortcut),
longName(_longName),
description(_description),
required(_required),
value(_value)
{}


//  ============================================================================= class Command Line

//  ------------------------------------------------------------------------------------ constructor
CommandLine::CommandLine(int argc, const char * argv[]) {

    numArguments = argc;
    
    for (auto i = 0; i < argc; i++) {
        originalArguments.emplace_back(argv[i]);
    }

    //  .................................................................... add default help switch
    clSwitches.emplace_back(CLswitch("h", "help", "Display help message", false, std::string() ) );
}


//  ------------------------------------------------------------------------------------ make switch
bool CommandLine::makeSwitch(std::string _shortcut, std::string _longName, std::string _description, bool _required, std::string _defaultValue) {

    if (_shortcut == "h" || _shortcut == "H" || _longName == "help" || _longName == "Help" || _longName == "HELP") {
        std::cout << "h, H, help, Help and HELP are reserved switch names - please use something else" << std::endl;
        return false;
    }

    if (_shortcut.empty() ) {
        std::cout << "Shortcut cannot be blank" << std::endl;
        return false;
    }

    if (_longName.empty() ) {
        std::cout << "Long name cannot be blank" << std::endl;
        return false;
    }

    while (_shortcut.front() == '-') {
        _shortcut.erase(_shortcut.front() );
    }

    while (_longName.front() == '-') {
        _longName.erase(_longName.front() );
    }

    auto pred = [ &_shortcut, &_longName ] (const CLswitch &s) { return( s.shortcut == _shortcut || s.longName == _longName ); };
    auto it = std::find_if(clSwitches.begin(), clSwitches.end(), pred);

    if (it != clSwitches.end() ) {
        std::cout << "makeSwitch called with a duplicate shortcut or longName - please make sure they are all unique!" << std::endl;
        return false;                                          //  conflicting switch already exists
    }

    clSwitches.emplace_back(CLswitch(_shortcut, _longName, _description, _required, _defaultValue) );
    return true;
}


//  ----------------------------------------------------------------------------- parse command line
bool CommandLine::parseCommandLine() {

    //  .............. working copy. Original preserved so that parse can be called again if desired
    std::vector<std::string> arguments = originalArguments;

    executablePath = arguments[0];
    int argIndex = 1;

    //  ..................................................... check how many parameters are REQUIRED
    int numRequiredParams = 0;
    for (const auto &s : clSwitches) {
        if (s.required) {
            numRequiredParams++;
        }
    }

    //  .............................................. check for no entry when something IS required
    if ( (numRequiredParams != 0) && (numArguments < 2) ) {
        displayHelp( std::to_string(numRequiredParams) +  " parameters are mandatory");
        return false;
    }

    //  ................................................... whilst there are arguments left to parse
    while (argIndex < numArguments) {

        //  ......................................... look for unlabeled parameter (first of a pair)
        if ( arguments[argIndex].front() != '-') {
            displayHelp("A switch '-x' must precede each value parameter. eg -t 4. Only simple switches can be consecutive eg -x -b");
            return false;
        }

        while (arguments[argIndex].front() == '-') {                        //  clip the '-' or '--'
            arguments[argIndex].erase(arguments[argIndex].begin() );
        }

        //  .............................................. check for a switch with missing id ('- ')
        if (arguments[argIndex].empty() ) {
            displayHelp( "switches starting with - cannot be blank" );
            return false;
        }

        //  ........................................................... check if user asked for help
        if ((arguments[argIndex] == "h")    ||
            (arguments[argIndex] == "H")    ||
            (arguments[argIndex] == "help") ||
            (arguments[argIndex] == "Help") ||
            (arguments[argIndex] == "HELP") )
        {
            displayHelp("Usage:" + arguments[argIndex] );
            return false;
        }

        //  .............................................................. find corresponding switch
        auto pred = [&] (const CLswitch &s) { return( s.shortcut == arguments[argIndex] || s.longName == arguments[argIndex] ); };
        auto it = std::find_if(clSwitches.begin(), clSwitches.end(), pred);

        //  ............................................................... check for invalid switch
        if (it == clSwitches.end() ) {
            displayHelp("Switch does not exist: -" + arguments[argIndex] );
            return false;
        }

        size_t clSwitchIndex = std::distance(clSwitches.begin(), it);
        argIndex++;


        //  .................................................... inspect next argument (if pressent)
        if (numArguments <= argIndex) {
            clSwitches[clSwitchIndex].value = "TRUE";           //  no next argument - set flag true

            if (clSwitches[clSwitchIndex].required) {
                numRequiredParams--;                                  //  decrement required counter
            }

        } else {                                                         // there are more arguments

            //  .......................... check if there is a next argument (or it's a bool switch)
            if (arguments[argIndex].front() == '-') {
                clSwitches[clSwitchIndex].value = "TRUE";              // next arg is another switch

                if (clSwitches[clSwitchIndex].required) {
                    numRequiredParams--;                              //  decrement required counter
                }

            } else {
                clSwitches[clSwitchIndex].value = arguments[argIndex];                 //  set value

                if (clSwitches[clSwitchIndex].required) {
                    numRequiredParams--;                              //  decrement required counter
                }

                argIndex++;
            }
        }
    }

    if (numRequiredParams != 0) {
        displayHelp("At least 1 required parameter has not been specified");
        return false;
    }

    return true;
}


//  ----------------------------------------------------------------------------------- display help
void CommandLine::displayHelp(std::string reason) {
    //  ................................................. find widths needed to display these params
    int lengthLongName = static_cast<int>( std::string("Long Name").length() );
    int lengthDescription = static_cast<int>( std::string("Description").length() );
    for (const auto &s : clSwitches) {
        lengthLongName = static_cast<int>( std::max(size_t(lengthLongName), s.longName.length() ) );
        lengthDescription = static_cast<int>( std::max(size_t(lengthDescription), s.description.length() ) );
    }

    //  ............................................................ add spacing above the max width
    lengthLongName += 3;        //  yep... ugly padding - allows for the '-'
    lengthDescription += 2;

    //  .................................................................................... heading
    std::cout << reason << std::endl << std::endl;

    std::cout << "Usage Help:" << std::endl;
    std::cout << std::left
    << std::setw(10) << "Shortcut"
    << std::setw(lengthLongName) << "Long name"
    << std::setw(lengthDescription) << "Description"
    << std::setw(10) << "Required"
    << "Value"
    << std::endl;

    std::cout << std::left
    << std::setw(10) << "--------"
    << std::setw(lengthLongName) << "---------"
    << std::setw(lengthDescription) << "-----------"
    << std::setw(10) << "--------"
    << "-----"
    << std::endl;

    //  .............................................................................. loop switches
    for (const auto &s : clSwitches) {
        std::cout << std::left
        << "-" << std::setw(9) << s.shortcut
        << "-" << std::setw(lengthLongName - 1) << s.longName
        << std::setw(lengthDescription) << s.description
        << std::setw(10) << ( (s.required) ? "required" : "optional")
        << s.value
        << std::endl;
    }
    std::cout << std::endl
    << "Values can be entered as:" << std::endl
    << "plain text:  Do NOT include any spaces.  'true' & 'false' are reserved for bools." << std::endl
    << "numbers:     eg 128 (an integer), 13.6 (a double), 1.3866E+06 (also a double)" << std::endl
    << "booleans:    enter 'true' or 'false' in full. Variations of 'true', 'True', 'TRUE' are all valid." << std::endl << std::endl;
}


//  ------------------------------------------ get switch values (overloaded by desired return type)

//  ......................................................................................... string
bool CommandLine::getSwitchValue(std::string _longName, std::string &switchValue) {

    auto pred = [&_longName] (const CLswitch &s) { return( s.longName == _longName ); };
    auto it = std::find_if(clSwitches.begin(), clSwitches.end(), pred);

    if (it == clSwitches.end() ) {
        switchValue.clear();
        return false;
    }

    size_t index = std::distance(clSwitches.begin(), it);
    switchValue = clSwitches[index].value;
    return true;
}


//  ........................................................................................... bool
bool CommandLine::getSwitchValue(std::string _longName, bool &switchValue) {

    auto pred = [&_longName] (const CLswitch &s) { return( s.longName == _longName ); };
    auto it = std::find_if(clSwitches.begin(), clSwitches.end(), pred);

    if (it == clSwitches.end() ) {
        switchValue = false;
        return false;
    }

    size_t index = std::distance(clSwitches.begin(), it);

    if (clSwitches[index].value == "true"   ||
        clSwitches[index].value == "True"   ||
        clSwitches[index].value == "TRUE")
    {
        switchValue = true;
        return true;

    } else if (clSwitches[index].value == "false"   ||
               clSwitches[index].value == "False"   ||
               clSwitches[index].value == "FALSE")
    {
        switchValue = false;
        return true;

    } else {
        switchValue = false;
        return false;
    }
}


//  ............................................................................................ int
bool CommandLine::getSwitchValue(std::string _longName, int &switchValue) {

    auto pred = [&_longName] (const CLswitch &s) { return( s.longName == _longName ); };
    auto it = std::find_if(clSwitches.begin(), clSwitches.end(), pred);

    if (it == clSwitches.end() ) {
        switchValue = int(0);
        return false;
    }

    size_t index = std::distance(clSwitches.begin(), it);

    try {
        switchValue = std::stoi(clSwitches[index].value);
        return true;

    } catch (...) {
        switchValue = int(0);
        return false;
    }
}


//  ......................................................................................... double
bool CommandLine::getSwitchValue(std::string _longName, double &switchValue) {

    auto pred = [&_longName] (const CLswitch &s) { return( s.longName == _longName ); };
    auto it = std::find_if(clSwitches.begin(), clSwitches.end(), pred);

    if (it == clSwitches.end() ) {
        switchValue = double(0.0);
        return false;
    }

    size_t index = std::distance(clSwitches.begin(), it);

    try {
        switchValue = std::stod(clSwitches[index].value);
        return true;

    } catch (...) {
        switchValue = double(0.0);
        return false;
    }
}

std::string CommandLine::getExecutablePath() {
    return executablePath;
}

//  ......................................................................................... toggle
bool CommandLine::getSwitchOnOff(std::string shortcut, bool &onOff) {

    std::string off = shortcut;
    std::transform(off.begin(), off.end(), off.begin(), ::tolower );

    std::string on = shortcut;
    std::transform(on.begin(), on.end(), on.begin(), ::toupper );

    bool matched = false;
    bool ok = true;
                                                                           //  find on in clSwitches
    auto predOn = [&] (const CLswitch &s) { return(s.shortcut == on); };
    auto itOn = std::find_if(clSwitches.begin(), clSwitches.end(), predOn );

    if (itOn != clSwitches.end() ) {                                // found on switch - test if set
        if (clSwitches[std::distance(clSwitches.begin(), itOn)].value == "TRUE") {
            onOff = true;
            matched = true;
        }
    }
                                                                          //  find off in clSwitches
    auto predOff = [&] (const CLswitch &s) { return (s.shortcut == off); };
    auto itOff = std::find_if(clSwitches.begin(), clSwitches.end(), predOff );

    if (itOff != clSwitches.end() ) {                             //  found off switch - test if set
        if ( clSwitches[std::distance(clSwitches.begin(), itOff)].value == "TRUE") {
            if (matched) {
                ok = false;
            }
            onOff = false;
        }
    }

    return ok;
}
