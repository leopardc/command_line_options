//
//  CommandLine.hpp
//  CommandLineOptions
//
//  Created by Leonard Coster on 7/2/21.
//

#ifndef CommandLine_hpp
#define CommandLine_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>    //  needed for GCC


class CommandLine {

public:

    //  -------------------------------------------------------------------------------- constructor
    CommandLine(int argc, const char * argv[]);

    //  ----------------------------------------------------------------------------- public methods
    bool makeSwitch(std::string _shortcut, std::string _longName, std::string _description, bool _required, std::string _default_value_as_string);
    bool parseCommandLine();
    void displayHelp(std::string reason);

    bool getSwitchValue(std::string longName, std::string &switchValue);
    bool getSwitchValue(std::string longName, bool &switchValue);
    bool getSwitchValue(std::string longName, int &switchValue);
    bool getSwitchValue(std::string longName, double &switchValue);

    bool getSwitchOnOff(std::string shortcut, bool &onOff);

    std::string getExecutablePath();

private:

    struct CLswitch {

        CLswitch();
        CLswitch(std::string _shortcut, std::string _longName, std::string _description, bool _required, std::string _value);

        std::string shortcut;
        std::string longName;
        std::string description;
        bool        required;
        std::string value;
    };
    std::vector<CLswitch> clSwitches;

    std::string executablePath;

    int numArguments;
    std::vector<std::string> originalArguments;
};

#endif /* CommandLine_hpp */
