# Introduction
CommandLine is a small class designed to simplify parsing of command line switches.

It was intended to find a balance between functionality and complexity.

# Dependencies
CommandLine depends only on the c++ 11 standard library.
There are no external dependencies.

# Methodology
Switches can be added programatically (see examples) and will be parsed against the default `argc, argv[]` with appropriate success/failure notification and suitable messages.

The values can then be obtained in variables of the desired type simply by declaring your variable and calling getSwitchValue.


# Usage
1. Add _CommandLine.cpp_ & _CommandLine.hpp_ to your project and  
`#include "CommandLine.hpp"`

2. create a _CommandLine object_, passing the argc/v parameters from main:  
`CommandLine cl(argc, argv);`
 
3. Create as many switches as you like using the _makeSwitch_ method:  
 `cl.makeSwitch("t", "threads", "desired number of threads", true, "4" );`   
 _makeSwitch_ returns a bool to indicate success or failure.  
  
4. Call _parseCommandLine_ on the object:  
`cl.parseCommandLine();`  
 _parseCommandLine_ returns a bool to indicate success or failure.
 
5. Variables are available in the type you want by calling _getSwitchValue_ which is overloaded with types: _string, bool, int & double_.  
_getSwitchValue_ returns a success bool, false if the variable is not convertable to your type.  
`int numThreads;`  
`bool success = cl.getSwitchValue("threads", numThreads);`

## Method details
### CommandLine constructor
The signature fo the constructor is:  
`CommandLine(int argc, const char * argv[])`  
It will capture the command line parameters and construct the default help switch (`-h help`) for you.  
You cannot add a help switch manually, a suitable error message will be produced if you try.

 
See the example below for usage.
### makeSwitch()
The signature of makeSwitch is:  
 `makeSwitch(std::string _shortcut, std::string _longName, std::string _description, bool _required, std::string _default_value_as_string)`
#### _shortcut
This is the shorter version of the switch, often just a single character but it can be as long as needed.  
#### _longName
The full switch name. The user may enter this or the shortcut, commandLine will recognise both.  
##### sortcut and long name features
The strings for the shortcut and longName do not need to include the `-` (eg `-t`) but it can be included for clarity if desired. commandLine tests the first character of the shortCut and longName to detect the `-`.

The longName is used in `getSwitchValue()` to uniquely identify the desired switch.  
As such, `makeSwitch()` tests each longName to ensure they are unique. It will return **false** when trying to add the switch if there is a naming conflict with any existing switches.
#### _description
The text you wish the help message to display to explain the function of the switch to users.  
The help display will automatically expand the width to accomodate the longest description and still keep the columns lined up.  
As short a description as practical is therefore optimal.
#### _required
A boolean indicating if the user must enter this parameter.  
`parseCommandLine()` will return false if any of the required switches are omitted.
#### _default value as string
In case the switch is omitted by the user, this default value will be used.  
The values should be entered as a `string`.  
CommandLine include an overloaded `getSwitchValue()` method that converts the string to the desired type.  
See below.
### parseCommandLine()
`parseCommandLine()` takes no parameters but used the `argc`, `argv` captured in the commandLine constructor and the switch data created with `makeSwitch()` to parse the switches entered by the user.  
Useful help messages will be displayed to help the user enter values correctly.
### getSwitchValue()
The signature for getSwitchValue is:
`getSwitchValue(std::string longName, <type> &switchValue)`
#### longName
Entered as a string, this is comapred to the longNames specified when creating the switches using `makeSwitch()`.  The longNames must be unique and makeSwitch will ensure this is the case.
#### switchValue
`getSwitchValue` is overloaded for 4 types: `std::string`, `int`, `double` & `bool`.

By passing a variable of the desired type, the correct overload will be selected and the value entered by the user will be converted to the desired type if possible.

The conversions are performed using `std::stoi` for integers, `std::stod` for doubles, including exponential representations and by verbose matching to the full text for the boolean states.  
The text 'true', 'True' or 'TRUE' will be returned as `bool true`.  
Similarly, 'false', 'False' or 'FALSE' will be returned as `bool false`.

`getSwitchValue` will return **true** or **false** to indicate if the desired conversion was possible.

See the example code for more details on usage.

### getSwitchOnOff()

The signature is:  
`bool getSwitchOnOff(std::string shortcut, bool &onOff)`

`getSwitchOnOff` looks for the presence of lower or upper case flags corresponding to OFF or ON.  

Set up switches for (say) `v` and `V`.  
If the user specifies `-v` then the onOff bool will be set **FALSE**  
If the user specifies `-V` then the onOff bool will be set **TRUE**

In both of these cases the method returns true to indicate success.  
If the switch is not found, or the user specifies BOTH (eg -v & -V) then the method returns false (and onOff will also be false).

# Full example
Also included in this repo as _main.cpp_.

```
//
//  main.cpp
//  CommandLineOptions
//
//  Created by Leonard Coster on 7/2/21.
//

#include <iostream>

/** ------------------------------------------------------------------------------------------------
 1. Include the CommandLine header.                                                               */

#include "CommandLine.hpp"


int main(int argc, const char * argv[]) {

    /** --------------------------------------------------------------------------------------------
     2. Declare a CommandLine object.                                                             */

    CommandLine cl(argc, argv);


    /** --------------------------------------------------------------------------------------------
     3. Add some switches.
        The method template will prompt you to enter the shortcut, longName, description, requried state & default values.
        makeSwitch returns a success bool and presents appropriate parsing errors in the console. */

    cl.makeSwitch("t", "threads", "desired number of threads", false, "4" );
    cl.makeSwitch("i", "includeFirst", "Include the first record", false, "FALSE");
    cl.makeSwitch("v", "verboseOff", "Turn verbose reporting off", false, "FALSE");
    cl.makeSwitch("V", "verboseOn", "Turn verbose reporting on", false, "TRUE");


    /** --------------------------------------------------------------------------------------------
     4. You don't have to (and will not be allowed) add help.                                     */

    if ( cl.makeSwitch("h", "help", "show me the help !", true, std::string() ) ) {
        //  returned true - the switch was added - but not in this case !
    } else {
        //  error message in console
    }


    /** --------------------------------------------------------------------------------------------
     5. Call parseCommandLine when ready.
        parseCommandLine returns a success bool and presents appropriate error messages
        for the user in the cosole.                                                               */

    cl.parseCommandLine();



    /** --------------------------------------------------------------------------------------------
     6. Access the data using the longName of the switch you want.
        The entries are stored as strings but converted to the type you want.
        If requesting a bool, getSwitchValue will look for 'true', 'True', 'TRUE', 'false', 'False'' & 'FALSE'
        Conversion to int or double is attempted with std::stoi or std::stod as appropriate.
        getSwitchValue returns true/false if the conversion to your desired type was successful.  */

    int numThreads;
    cl.getSwitchValue("threads", numThreads);

    std::string dataset;
    cl.getSwitchValue("dataset", dataset);

    bool includeFirst;
    cl.getSwitchValue("includeFirst", includeFirst);

    bool verboseOnOff;
    cl.getSwitchOnOff("v", verboseOnOff);

    std::string x = cl.getExecutablePath();

    return 0;
}
```

# Notes
* You do not need to add the '-' when specifying the shortcut or long name in makeSwitch. This will be added for you.
* The argument data is always captured as string but you can request whatever type you like using the overloaded getSwitchValue method.
* If getting the user entered value (rather than the default you optionally provided in *makeSwitch*) is important to you then be sure to check the bool return from *getSwitchValue* to know if the conversion to your desired type was possible.
* A help message will automatically be displayed to the user if:  
 a) if they enter -h or -help  
 b) if they miss a paramter marked as 'required'  
 c) if a paramter without a flag label is entered (eg `4`, rather than `-t 4` )
* Solo and consecutive flags ( `-x -t` ) are permitted and will be captured as 'true'.  They are actually stored as the sting literal  
ie the flag ***IS*** present.
* Following this logic you may want to turn flags on/off by having the user enter `-t` for off an `-T` for on.  
In this case simply set the default for both to *false* and check which one is *true* after parsing. 

# Change Log
#### 09 07 2021
* Added handling for multiple dashes ('-') both in `makeSwitch()` and `parseCommandLine()`

#### 06 08 2021  
* Added `getSwitchOnOff` for handling on/off toggles more easily.
* Fixed bug where the last parameter as a single switch might cause issues parsing into "nothing"
